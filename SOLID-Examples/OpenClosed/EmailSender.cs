﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosed
{
    public class Student
    {
        private string name;
        public Student(string stu_name = "")
        {
            name = stu_name;
        }
    }
    public class StudentList
    {
        private List<Student> slist = new List<Student>();
        public int Size { get; private set; }

        public void Add(Student stu)
        {
            slist.Add(stu);
            Size = slist.Count;
        }
    }

    // what if we now also want to support html email body?
    public class EmailSender
    {
        public void SendEmail(StudentList stu_list, string content)
        {
            Console.WriteLine("Sent email to " + stu_list.Size + " student(s).");
        }
    }
}
