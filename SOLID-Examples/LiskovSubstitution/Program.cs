﻿using System;

namespace LiskovSubstitution
{
    class Program
    {
        static void Main(string[] args)
        {
            var rec = new Rectangle();
            //var rec = new Square();
            rec.Width = 10;
            rec.Height = 20;
            Console.WriteLine("The area is " + rec.Area());

        }
    }
}
