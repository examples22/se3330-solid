﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitution
{
    public class Rectangle
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public virtual int Area() { return this.Width * this.Height; }
    }

    public class Square : Rectangle
    {
        public override int Area()
        {
            return this.Width * this.Width;
        }
    }
}
