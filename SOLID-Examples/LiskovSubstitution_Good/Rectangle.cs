﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitution_Good
{
    public interface Shape
    {
        public int Area();
    }
    public class Rectangle : Shape
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public Rectangle(int w, int h)
        {
            this.Width = w;
            this.Height = h;
        }
        public int Area() { return this.Width * this.Height; }
    }

    public class Square : Shape
    {
        public int Width { get; set; }
        public Square(int w)
        {
            this.Width = w;
        }
        public int Area()
        {
            return this.Width * this.Width;
        }
    }
}
