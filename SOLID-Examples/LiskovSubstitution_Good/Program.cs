﻿using System;

namespace LiskovSubstitution_Good
{
    class Program
    {
        static void Main(string[] args)
        {
            var rec = new Rectangle(10,20);
            //var rec = new Square(10);
            Console.WriteLine("The area is " + rec.Area());
        }
    }
}
