﻿using System;

namespace Single_Responsibility_Good
{
    class Program
    {
        static void Main(string[] args)
        {
            var stu_list = new StudentList();
            stu_list.Add(new Student("Joyce King"));

            var email_sender = new EmailSender();
            var content = "Test email. Please ignore.";
            email_sender.SendEmail(stu_list, content);
        }
    }
}
