﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Single_Responsibility_Good
{
    public class Student
    {
        private string name;
        public Student(string stu_name = "")
        {
            name = stu_name;
        }
    }
    public class StudentList
    {
        private List<Student> slist = new List<Student>();
        public int Size { get; private set; }

        public void Add(Student stu)
        {
            slist.Add(stu);
            Size = slist.Count;
        }
    }

    // separate the responsbility of sending email from maintaining a student list
    public class EmailSender
    {
        public void SendEmail(StudentList stu_list, string content)
        {
            Console.WriteLine("Sent email to " + stu_list.Size + " student(s).");
        }
    }
}
