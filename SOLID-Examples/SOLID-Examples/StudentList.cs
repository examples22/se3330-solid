﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Single_Responsibility
{
    public class Student
    {
        private string name;
        public Student(string stu_name = "")
        {
            name = stu_name;
        }
    }

    // There are two reasons to change this class:
    // 1) What if we only want to provide student name when adding a student to a list?
    // 2) What if we want to change the email protocol?
    public class StudentList
    {
        private List<Student> slist = new List<Student>();
        private int size = 0;
        
        public void Add(Student stu)
        {
            slist.Add(stu);
            size = slist.Count;
        }

        public void SendEmail(string content)
        {
            Console.WriteLine("Sent text email to " + size + " student(s).");
        }
    }
}
