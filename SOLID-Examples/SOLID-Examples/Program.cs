﻿using System;

namespace Single_Responsibility
{
    class Program
    {
        static void Main(string[] args)
        {
            var stu_list = new StudentList();
            stu_list.Add(new Student("Joyce King"));
            stu_list.SendEmail("Test email. Please ignore.");
        }
    }
}
