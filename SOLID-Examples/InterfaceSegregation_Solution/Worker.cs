﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceSegregation_Solution
{
    class Manager
    {
        private IWorkable worker;
        public void SetWorker( IWorkable w)
        {
            worker = w;
        }

        public void Manage()
        {
            worker.Work();
        }
    }

    interface IWorkable
    {
        public void Work();
    }

    interface IFeedable
    {
        public void Eat();
    }
    class Worker : IWorkable, IFeedable
    {
        public void Work()
        {
            Console.WriteLine("Worker is working...");
        }

        public void Eat()
        {
            Console.WriteLine("Worker is eating lunch...");
        }
    }

    class SuperWorker : IWorkable, IFeedable
    {
        public void Work()
        {
            Console.WriteLine("Super worker is working super hard...");
        }

        public void Eat()
        {
            Console.WriteLine("Sper worker is eating a huge lunch...");
        }
    }

    // What if your company now has a new type of worker: Robot?
    // Robot works but doesn't eat...
    class Robot : IWorkable
    {
        public void Work()
        {
            Console.WriteLine("Robot is working...");
        }

        public void Eat()
        {
            throw new Exception("Robot is broken because robot doesn't eat human food.");
        }
    }


}
