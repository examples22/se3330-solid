﻿using System;

namespace OpenClosed_Good
{
    class Program
    {
        static void Main(string[] args)
        {
            var stu_list = new StudentList();
            stu_list.Add(new Student("Joyce King"));

            var email_sender = new EmailSender();

            //var content = new TextContent("Test email. Please ignore.");
            var content = new HtmlContent("<h1>header</h1><br><p>Test email. Please ignore.</p>");
            email_sender.SendEmail(stu_list, content);
        }
    }
}
