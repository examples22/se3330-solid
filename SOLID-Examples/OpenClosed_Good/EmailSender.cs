﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosed_Good
{
    public class Student
    {
        private string name;
        public Student(string stu_name = "")
        {
            name = stu_name;
        }
    }
    public class StudentList
    {
        private List<Student> slist = new List<Student>();
        public int Size { get; private set; }

        public void Add(Student stu)
        {
            slist.Add(stu);
            Size = slist.Count;
        }
    }

    public class EmailSender
    {
        public void SendEmail(StudentList stu_list, IContent content)
        {
            Console.WriteLine("Sent email to " + stu_list.Size + " student(s).");
        }
    }

    // introduce a buffer, interface IContent, to make sure
    // if we need to support more content format in the future,
    // existing EmailSender class doesn't need to be changed.
    public interface IContent
    {
        public string Content { get; set; }
    }

    public class TextContent : IContent
    {
        public string Content { get; set; }
        public TextContent(string content)
        {
            this.Content = content;
        }
    }

    public class HtmlContent : IContent
    {
        public string Content { get; set; }

        public HtmlContent(string html_string)
        {
            // this should be a different implementation to serialize html to string
            this.Content = html_string;
        }
    }

}
