﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversion_Solution
{
    interface IWorker
    {
        public void Work();
    }
    class Worker : IWorker
    {
        public void Work()
        {
            Console.WriteLine("Worker is working...");
        }
    }

    class Manager
    {
        private IWorker worker;

        public Manager(IWorker w)
        {
            worker = w;
        }
        public void Manage()
        {
            worker.Work();
        }
    }

    // What if now we want to add a SuperWorker class
    // and the manager will now manager super worker?
    // We don't need to change Manager class.
    class SuperWorker : IWorker
    {
        public void Work()
        {
            Console.WriteLine("Super worker is working super hard...");
        }
    }

}
