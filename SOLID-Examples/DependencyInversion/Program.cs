﻿using System;

namespace DependencyInversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager m = new Manager(new Worker());
            // Manager m = new Manager(new SuperWorker());
            m.Manage();
        }
    }
}
