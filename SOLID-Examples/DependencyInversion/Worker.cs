﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversion
{
    class Worker
    {
        public void Work()
        {
            Console.WriteLine("Worker is working...");
        }
    }

    class Manager
    {
        private Worker worker;

        public Manager(Worker w)
        {
            worker = w;
        }
        public void Manage()
        {
            worker.Work();
        }
    }

    // What if now we want to add a SuperWorker class
    // and the manager will now manager super worker?
    // We will need to change Manager class.
    class SuperWorker
    {
        public void Work()
        {
            Console.WriteLine("Super worker is working super hard...");
        }
    }

}
