﻿using System;

namespace InterfaceSegregation
{
    class Program
    {
        static void Main(string[] args)
        {
            Manager m = new Manager();
            m.SetWorker(new Robot());
            m.Manage();
        }
    }
}
