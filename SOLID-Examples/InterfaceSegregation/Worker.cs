﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceSegregation
{
    class Manager
    {
        private IWorker worker;
        public void SetWorker( IWorker w)
        {
            worker = w;
        }

        public void Manage()
        {
            worker.Work();
        }
    }

    interface IWorker
    {
        public void Work();
        public void Eat();
    }
    class Worker : IWorker
    {
        public void Work()
        {
            Console.WriteLine("Worker is working...");
        }

        public void Eat()
        {
            Console.WriteLine("Worker is eating lunch...");
        }
    }

    class SuperWorker : IWorker
    {
        public void Work()
        {
            Console.WriteLine("Super worker is working super hard...");
        }

        public void Eat()
        {
            Console.WriteLine("Sper worker is eating a huge lunch...");
        }
    }

    // What if your company now has a new type of worker: Robot?
    // Robot works but doesn't eat...
    
}
