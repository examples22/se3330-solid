﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Single_Responsibility_Exercise
{
    class Program
    {
        static void Main(string[] args)
        {
            var user = new User();
            var db = new Database();

            string post = "This is my first post!";
            user.CreatePost(db, post);
        }
    }
}
