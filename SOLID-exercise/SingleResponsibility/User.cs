﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// A class has one and only one reason to change!

namespace Single_Responsibility_Exercise
{
    public class Database
    {
        private List<string> posts = new List<string>();
        private List<string> error_logs = new List<string>();

        public void Add(string new_post)
        {
            posts.Add(new_post);
        }

        public void LogError (string error_message)
        {
            DateTime dt = DateTime.Now;
            error_logs.Add(dt.ToString() + ": " + error_message);
        }
    }

    // How many responsibilities does User class have?
    public class User
    {
        public void CreatePost(Database db, string post_message)
        {
            try 
            { 
                db.Add(post_message);
                Console.WriteLine(post_message);
            }
            catch (Exception ex)
            {
                db.LogError(ex.ToString());
                Console.WriteLine(ex.ToString());
            }
        }

    }
}
