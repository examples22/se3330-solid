﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Violation of Liskov Substituition Principle usually indicate inproper inheritance relationship.
// The common fix is to define a higher level base class and
// let the current base and derived classes both inherit the new base class.

namespace LiskovSubstitution
{
    class Employee
    {
        public const decimal BASE = 16;
        public const decimal STEP = 3;

        public string Name { get; set; }
        public int Rank { get; set; }

        public virtual decimal PayRate()
        {
            return BASE + (Rank - 1) * STEP;
        }
    }

    class CEO : Employee
    {
        public decimal AnnualSalary { get; set; }
        public override decimal PayRate()
        {
            throw new InvalidOperationException("CEO doesn's have a payrate.");
        }
    }
}
