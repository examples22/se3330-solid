﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitution
{
    class Program
    {
        static void Main(string[] args)
        {
            // If it works for base class, it should work in exactly the same way with a derived class.
            var employee = new Employee();
            //var employee = new CEO();
            employee.Name = "John Smith";
            employee.Rank = 8;
            Console.WriteLine(employee.Name + " has a pay rate of $" + employee.PayRate() + "/hour.");
        }
    }
}
