﻿using System;

namespace DependencyInversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Store store = new Store();
            store.Purchase("Ava", "cup", 3.99, 4);
        }
    }
}
