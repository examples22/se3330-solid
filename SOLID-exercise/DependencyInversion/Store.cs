﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversion
{
    // What if now the store also accepts QR payment?
    class Store
    {
        private Stripe stripe = new Stripe();

        public void Purchase(string user, string product, double price, int count)
        {
            double total = price * count;
            Console.WriteLine(user + " bought " + count + " " + product + "(s) for " + price + " each.");
            stripe.ProcessPayment(user, total);
        }
    }

    class Stripe
    {
        public void ProcessPayment(string user, double amount)
        {
            // actual stripe API taking care of payment
            Console.WriteLine(user + " made a payment of " + amount + " using stripe.");
        }
    }



}
