﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenClosed
{

    // HINT: create a Question interface as a buffer
    // between different types of questions and the quiz generator.
    // The only thing QuizGenerator needs from Question interface is 
    // how to print a specific type of questions.

    class BooleanQuestion 
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public BooleanQuestion()
        {
            this.Type = "bool";
        }
    }

    class MultipleChoiceQuestion
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public List<string> Options { get; set; }

        public MultipleChoiceQuestion( ) 
        {
            this.Type = "multiple_choice";
        }
    }

    class EssayQuestion
    {
        public string Type { get; set; }
        public string Description { get; set; }
        public EssayQuestion()
        {
            this.Type = "essay";
        }
    }
}
