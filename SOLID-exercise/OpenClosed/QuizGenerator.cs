﻿using System;
using System.Collections.Generic;

// A class should be open for extension but close for modification:
// we can add new features. But during this process, we should NOT modify existing class.
namespace OpenClosed
{
    // If we are adding a new type of question (Matching),
    // we need to change the QuizGenerator class by adding a new PrintMatchingQuestion method
    class QuizGenerator
    {
        public void PrintBooleanQuestion(BooleanQuestion q)
        {
            Console.WriteLine(q.Description);
            Console.WriteLine("1. True\t2. False");
        }

        public void PrintMultipleChoiceQuestion(MultipleChoiceQuestion q)
        {
            Console.WriteLine(q.Description);
            for (int i = 1; i <= q.Options.Count; i++)
                Console.WriteLine(i.ToString() + ". " + q.Options[i - 1]);
        }

        public void PrintEssayQuestion(EssayQuestion q)
        {
            Console.WriteLine(q.Description);
            Console.WriteLine("\n\n\n\n");
        }

        static void Main(string[] args)
        {
            // different quesitons
            var boolq = new BooleanQuestion();
            boolq.Description = "SOLID are 5 very important OO design principles.";

            var mulq = new MultipleChoiceQuestion();
            mulq.Description = "What is your favorite SOLID principle?";
            List<string> options = new List<string>();
            options.Add("Single Responsibility");
            options.Add("Open Closed");
            options.Add("Liskov Substitution");
            options.Add("Interface Segragation");
            options.Add("Dependancy Inversion");
            mulq.Options = options;

            var essayq = new EssayQuestion();
            essayq.Description = "What is Open Closed principle?";

            var quiz = new QuizGenerator();
            quiz.PrintBooleanQuestion(boolq);
            Console.WriteLine();
            quiz.PrintMultipleChoiceQuestion(mulq);
            Console.WriteLine();
            quiz.PrintEssayQuestion(essayq);

        }
    }
}
