﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceSegregation
{
    // We have a fat/pollutted interface
    // Fix: seperate it into several smaller interfaces, 
    // each is only responsible for doing one thing.
    interface Order
    {
        public void OrderEntree();
        public void OrderSide();
        public void OrderDrink();
    }

    class BigMacComboOrder : Order
    {
        public void OrderEntree()
        {
            Console.WriteLine("Ordered a BigMac burger.");
        }
        public void OrderSide()
        {
            Console.WriteLine("Ordered a fries.");
        }
        public void OrderDrink()
        {
            Console.WriteLine("Ordered a Fanta.");
        }
    }

    // FishSandwith is forced to implement OrderSide and OrderDrink
    class FishSandwitchOrder : Order
    {
        public void OrderEntree()
        {
            Console.WriteLine("Ordered a Fish Sandwitch.");
        }
        public void OrderSide()
        {
            throw new Exception("No side is ordered.");
        }
        public void OrderDrink()
        {
            throw new Exception("No drink is ordered.");
        }
    }

}
