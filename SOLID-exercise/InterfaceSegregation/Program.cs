﻿using System;

namespace InterfaceSegregation
{
    class Program
    {
        static void Main(string[] args)
        {
            var order = new BigMacComboOrder();
            order.OrderEntree();
            order.OrderSide();
            order.OrderDrink();
        }
    }
}
